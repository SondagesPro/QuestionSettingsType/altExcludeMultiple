<?php
/**
 * Description
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017-2021 Denis Chenu <https://www.sondages.pro>
 * @copyright 2017 DIALOGS <https://dialogs.ca/>
 * @license AGPL
 * @version 0.1.0
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class altExcludeMultiple extends PluginBase
{
  static protected $name = 'altExcludeMultiple';
  static protected $description = 'Exclusive option without null';

  /**
  * Add function 
  */
  public function init() {
    $this->subscribe('afterPluginLoad'); /* translation */
    
    $this->subscribe('newQuestionAttributes','altExcludeMultipleAttribute');
    $this->subscribe('beforeQuestionRender','setExcludeMultiple');
    $this->subscribe('beforeSurveyPage','removeExcluded');

  }

  /**
   * Adding the constructed Question attribute settings
   */
  public function altExcludeMultipleAttribute()
  {
    $questionAttributes = array(
      'altExcludeMultiple'=>array(
        "types"=>"M",
        'category'=>gT('Logic'),
        'sortorder'=>101,
        'inputtype'=>'text',
        "help"=>$this->_translate("Excludes all other options if a certain answer is selected. Ensures that other responses are “not selected” in the database rather than “null”. Just enter the answer code(s) separated by a semicolon."),
        "caption"=>$this->_translate('Exclusive option (other choices not selected)'),
      ),
    );
    $this->getEvent()->append('questionAttributes', $questionAttributes);
  }

  /**
   * Set the javascript solution and add in SESSION
   */
  public function setExcludeMultiple()
  {
    $oEvent = $this->event;
    if($oEvent->get('type')=="M") {
      $aAttributes=QuestionAttribute::model()->getQuestionAttributes($this->getEvent()->get('qid'));
      if(trim($aAttributes['altExcludeMultiple'])) {
        $qid=$oEvent->get('qid');
        $sgqa=$oEvent->get('surveyId')."X".$oEvent->get('gid')."X".$oEvent->get('qid');
        $sAltExcludeMultiples = trim($aAttributes['altExcludeMultiple']);
        $aAltExcludeMultiples = array_filter(explode(";",$sAltExcludeMultiples));
        $assetUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/altExcludeMultiple.js');
        App()->clientScript->registerScriptFile($assetUrl);
        foreach($aAltExcludeMultiples as $sAltExcludeMultiple) {
          /* @todo : filter {$sAltExcludeMultiple} */
          App()->clientScript->registerScript("altExcludeMultiple{$qid}{$sAltExcludeMultiple}","altExcludeMultiple({$qid},'answer{$sgqa}{$sAltExcludeMultiple}');",CClientScript::POS_END);
          $controlField=\CHtml::hiddenField("altExcludeMultiple[$sgqa][]" , $sgqa.$sAltExcludeMultiple);
          $oEvent->set('answers',$oEvent->get('answers').$controlField);
        }
      }
    }
  }

  /**
   * Remove excluded part when submit
   */
  public function removeExcluded()
  {
    $altExcludeMultiplePosted=App()->getRequest()->getPost('altExcludeMultiple');
    if(!empty($altExcludeMultiplePosted) && is_array($altExcludeMultiplePosted)) {
      foreach($altExcludeMultiplePosted as $sgq=>$altExcludeMultipleSgq) {
        if(!empty($altExcludeMultipleSgq) && is_array($altExcludeMultipleSgq)) {
          foreach($altExcludeMultipleSgq as $excludeOther) {
            if(App()->getRequest()->getPost($excludeOther)=="Y") {
              foreach($_POST as $key => $value) {
                if ($key!=$excludeOther && strpos($key, $sgq) === 0 && $value) {
                  $_POST[$key]="";
                }
              }
            }
          }
        }
      }

    }
  }
    
  /** Translation part **/

  /**
   * Translate a string by this plugin
   * @param string
   * @return string
   */
  private function _translate($string){
      return Yii::t('',$string,array(),'message'.get_class($this));
  }
  /**
   * Add this translation just after loaded all plugins
   * @see event afterPluginLoad
   */
  public function afterPluginLoad() {
      // messageSource for this plugin:
      $pluginMessageSource=array(
          'class' => 'CGettextMessageSource',
          'cacheID' => get_class($this).'Lang',
          'cachingDuration'=>3600,
          'forceTranslation' => true,
          'useMoFile' => true,
          'basePath' => __DIR__ . DIRECTORY_SEPARATOR.'locale',
          'catalog'=>'messages',// default from Yii
      );
      Yii::app()->setComponent('message'.get_class($this),$pluginMessageSource);
  }
}
