/**
 * @file altExcludeMultiple javascript function
 * @author Denis Chenu
 * @copyright Denis Chenu <http://www.sondages.pro>
 * @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL v3.0
 */

function altExcludeMultiple(qid, exclude) {

  // All checkboxes in the question container
  $("#question" + qid + " .checkbox-item").on('change', function () {

    // Get the hidden input field
    var input = $(this).children("input:hidden");

    // Check if the hidden input field is one to exclude
    if (input.attr('id') === 'java' + exclude) {

      // If the hidden input field is not checked
      if (input.val() !== "Y") {
        // console.log('Disabled')

        // Disables all checkboxes if the exclusivity checkbox is checked
        $(this).siblings(".checkbox-item").find(":checkbox").not("#answer" + exclude).filter(':checked').trigger('click');
        $(this).siblings(".checkbox-item").find(":checkbox").not("#answer" + exclude).attr("disabled", true);
        $(this).siblings(".checkbox-item").addClass('ls-disabled');

        // Fix for the 'Other' option
        $(this).siblings(".checkbox-text-item").find(":checkbox").not("#answer" + exclude).filter(':checked').trigger('click');
        $(this).siblings(".checkbox-text-item").find(":checkbox").not("#answer" + exclude).attr("disabled", true);
        $(this).siblings(".checkbox-text-item").addClass('ls-disabled');

      } else {
        // console.log('Enabled')

        // Enables all checkboxes if the exclusivity checkbox is unchecked
        $(this).siblings(".checkbox-item").find(":checkbox").not("#answer" + exclude).attr("disabled", false);
        $(this).siblings(".checkbox-item").removeClass('ls-disabled');

        // Fix for 'Other' option
        $(this).siblings(".checkbox-text-item").find(":checkbox").not("#answer" + exclude).attr("disabled", false);
        $(this).siblings(".checkbox-text-item").removeClass('ls-disabled');
      }
    }
  });

}
